import React from 'react';
import yaml from 'yaml-js';
import styled from 'styled-components';
import { useTable } from 'react-table';

const Styles = styled.div`
    padding: 1rem;
    table {
        border-spacing: 0;
        border: 1px solid black;
        tr {
            :last-child {
                td {
                    border-bottom: 0;
                }
            }
        }
        th,
        td {
            margin: 0;
            padding: 0.1rem;
            border-bottom: 1px solid black;
            border-right: 1px solid black;
            :last-child {
                border-right: 0;
            }
        }
    }
    .filterInput {
        width: 50%;
    }
    .highlight {
        background-color: yellow;
    }
`;

function handleCell(cell) {
    if (cell.props.cell.value) {
        if (cell.props.cell.value.tags.includes('done')) {
            return cell.props.cell.value.tags.join(' ');
        } else {
            return cell.props.cell.value.tags.join(' ');
        }
    } else {
        return cell;
    }
}

function transformCell(cell) {
    if (typeof cell.props.cell.value === 'string') {
        return cell;
    } else {
        return handleCell(cell);
    }
}

function determineCellColor(cell) {
    if (typeof cell.props.cell.value === 'string') {
        return '#ffffff';
    } else {
        if (!cell.props.cell.column.modules
            .includes(cell.props.cell.row.values.Module)
        ) {
            return 'linear-gradient(to left top, transparent 47.75%, currentColor 49.5%, currentColor 50.5%, transparent 52.25%)';
        } else if (cell.props.cell.value) {
            if (cell.props.cell.value.tags.includes('done')) {
                if (cell.props.cell.value.tags.length !== 1) {
                    return 'yellow';
                }
                return '#82FF7D';
            } else if (true) {
                return 'pink';
            }
        } else {
            return '#ffffff';
        }
    }
}

function Table({ columns, data }) {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = useTable(
        {
            columns,
            data,
        },
    );

    return (
        <table {...getTableProps()}>
            <thead>
                {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map(column => (
                            <th {...column.getHeaderProps()}>
                                {column.render('Header')}
                            </th>
                        ))}
                    </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {rows.map(
                    (row, i) =>
                        prepareRow(row) || (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <td
                                            {...cell.getCellProps()}
                                            style={{
                                                background: determineCellColor(
                                                    cell.render('Cell')
                                                ),
                                            }}
                                        >
                                            {transformCell(cell.render('Cell'))}
                                        </td>
                                    );
                                })}
                            </tr>
                        )
                )}
            </tbody>
        </table>
    );
}

const ConfigUrl =
    'https://gitlab.com/api/v4/projects/14720879/repository/files/config.yaml?ref=master';

function fetchYaml(url) {
    return fetch(url)
        .then(response => response.json())
        .then(responseJson => ({id: url.match(/\d+/g)[1], ...yaml.load(atob(responseJson.content))}))
        .catch(error => ({name: url.match(/\d+/g)[1], error: error}));
}

function fetchConfig() {
    return fetchYaml(ConfigUrl);
}

function fetchMember(id) {
    return fetchYaml('https://gitlab.com/api/v4/projects/' +
        id + '/repository/files/exercises.yaml?ref=master')
}

function determineIfShowing(id, ids) {
    if (ids.includes(id)) {
        return false;
    } else {
        return true;
    }
}

function flatten(arr) {
    return arr.reduce(function (flat, toFlatten) {
        return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
    }, []);
}

function findExercises(member) {
    if (!member.exercises) { return null }
    var member_exercises_with_tags = [];
    return [[...new Set(flatten(member.modules.map(module =>
            member.exercises.map(entry =>
            entry[module] ? Object.keys(entry[module]).map(ex => {
                member_exercises_with_tags.push(
                ({term: entry.term,
                  week: entry.week,
                  module: module,
                  exercise: ex,
                  tags: entry[module][ex].tags})
                );
                return ({term: entry.term, week: entry.week, module: module, exercise: ex})
            }) : null)))
            .filter(x => x))], member_exercises_with_tags];
}

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            config: null,
            hiddenIDs: [],
            columns: [],
            rows: [],
            filter: {
                Module: [],
                Term: [],
                Week: [],
                Exercise: []
            }
        };
    }

    componentDidMount() {
        fetchConfig()
        .then((config) => {
            this.setState({ config: config });
            Promise.all(
                config.ids.map((id) => fetchMember(id))
            ).then((data) => {
                data.forEach((object) => {
                    this.setState({ data: this.state.data.concat([object]) })
                })
            })
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            this.state.config !== prevState.config ||
            this.state.data !== prevState.data
        ) {

            // Traverse members and build union of modules and exercises
            const member_exercises = {};
            const member_tags = {};

            this.state.data.map(member => {
                const tmp = findExercises(member);
                if (tmp) {
                    member_exercises[member.id] = tmp[0];
                    member_tags[member.id] = tmp[1];
                } else {
                    member_exercises[member.id] = tmp;
                    member_tags[member.id] = tmp;
                }
                return null
            });

            const _exercises = [...new Set(Array.prototype.concat(
                ...Object.values(member_exercises)))].filter(x => x);

            const exercises = _exercises.filter((thing, index) =>
                    {
                        return index === _exercises.findIndex(obj => {
                            return JSON.stringify(obj) === JSON.stringify(thing);
                        })
                    });

            const _rows = exercises.map(e => [
                e.module,
                e.term,
                e.week.toString(),
                e.exercise,
                ...this.state.data.map(member => {
                        return member_tags[member.id] ? member_tags[member.id].filter(val => {
                            return (val.term === e.term &&
                                    val.week === e.week &&
                                    val.module === e.module &&
                                    val.exercise === e.exercise)
                        })[0] : undefined
                    })
            ]);

            const _columns = [
                ['Module', [], 'Module'],
                ['Term', [], 'Term'],
                ['Week', [], 'Week'],
                ['Exercise', [], 'Exercise'],
                ...this.state.data.map(x => [x.name,
                    x.modules ? x.modules : [],
                    x.id ? x.id : ''])
            ];

            const columns = [{
                Header: 'Problems',
                accessor: 'Problems',
                columns: _columns.slice(0, 4).map(c => ({
                    Header: c[0],
                    accessor: c[0],
                    modules: c[1],
                    id: c[2],
                }))
                },
                {
                Header: 'People',
                accessor: 'People',
                columns: _columns.slice(4, _columns.length).map(c => ({
                    Header: c[0],
                    accessor: c[0],
                    modules: c[1],
                    id: c[2],
                }))
                }];

            const rows = _rows.map(_row =>
                _columns.reduce(
                    (acc, val, ind) => Object.assign(acc, { [val[0]]: _row[ind] }),
                    {}
                )
            );

            this.setState(() => ({ columns: columns, rows: rows }));
        }
    }

    render() {
        if (!this.state.config || !this.state.data[this.state.config.ids.length - 1]) {
            return (
                <div>Fetching data from {
                    this.state.config ? this.state.config.ids.length : '?'
                } source(s)...</div>
            );
        }
        const columns = this.state.columns.map(group => ({...group,
                        columns: group.columns.map(column =>
                            ({...column, show: determineIfShowing(
                                column.id, this.state.hiddenIDs)
                            }))}));
        const data = this.state.rows.filter(row =>
            Object.keys(this.state.filter).map(key =>
                Array.isArray(this.state.filter[key]) && this.state.filter[key].length ?
                this.state.filter[key].includes(row[key]) :
                true // If no constraints, then allow

            ).every(exp => exp)
        );
        return (
            <div>
                Filter modules:
                <input type="text"
                 placeholder="all"
                 value={this.state.filter.Module} onChange={
                    (event) => this.setState({filter: ({
                        ...this.state.filter,
                        Module: event.target.value ?
                        event.target.value.split(',').map(x => x.trim()) :
                        []
                    })
                })}/>
                <br/>
                Filter weeks:
                <input type="text"
                 placeholder="all"
                 value={this.state.filter.Week} onChange={
                    (event) => this.setState({filter: ({
                        ...this.state.filter,
                        Week: event.target.value ?
                        event.target.value.split(',').map(x => x.trim()) :
                        []
                    })
                })}/>
                <br/>
                Filter exercises:
                <input
                 placeholder="all"
                 type="text" value={this.state.filter.Exercise} onChange={
                    (event) => this.setState({filter: ({
                        ...this.state.filter,
                        Exercise: event.target.value ?
                        event.target.value.split(',').map(x => x.trim()) :
                        []
                    })
                })}/>
                <br/>
                <Styles>
                    <Table
                        columns={columns}
                        data={data}
                    />
                </Styles>
                <ul>
                    {this.state.data.map(x => (
                        <li>
                        {x.name}&nbsp;
                        {x.error ? x.error.problem.toString() + ' ' : ''}
                        <button onClick={
                            () => this.setState({hiddenIDs:
                                !this.state.hiddenIDs.includes(x.id) ?
                                [...this.state.hiddenIDs, x.id] :
                                this.state.hiddenIDs.filter(e =>
                                    e !== x.id
                                )
                            })
                        }>
                        {this.state.hiddenIDs.includes(x.id) ? 'Show' : 'Hide'}
                        </button>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}
